import 'package:flutter/material.dart';

//paa colors
const Color primaryColor = Color(0xff31bFB5);
const Color colorAccent = Color(0xff03FFFFFF);

//background colors
const Color backgroundColor = Color(0xff277E86);
const Color appBarBackgroundColor = Color(0xffFFFFFF);

//icons colors
const Color navigationIconColor = Color(0xff1C1F2A);

//text colors
const Color appNormalTextColor = Color(0xff1C1F2A);
const Color appWhiteTextColor = Color(0xffFFFFFF);
const Color appTextHintColor = Color(0xffffffff);
const Color appTextBorderColor = Color(0xffEDEFF0);
const Color appTextFieldBackgroundColor = Color(0xff278E86);
//
const Color textFieldGradientColorStart = Color(0xff1A5A46);
const Color textFieldGradientColorEnd = Color(0xff1A4A46);
const Color buttonGradientColorStart = Color(0xff56C385);
const Color buttonGradientColorEnd = Color(0xff41BFB5);
const Color backgroundGradientColorStart = Color(0xff0B3138);
const Color backgroundGradientColorEnd = Color(0xff0A4A46);

typedef void EmptyCallback();
typedef void TextCallback(String text);
typedef void BoolCallback(bool value);

