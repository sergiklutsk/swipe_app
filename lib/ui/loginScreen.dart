import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swipeapp/styles/appStyle.dart';
import 'package:swipeapp/styles/buttonStyles.dart';
import 'package:swipeapp/styles/snackBarStyles.dart';
import 'package:swipeapp/styles/textFieldStyles.dart';

final scaffoldKey = GlobalKey<ScaffoldState>();

String _phone = "";
String _smsCode = "";
bool _isSecondPage = false;

var textControllerPhone = new TextEditingController();

class LoginScreen extends StatefulWidget {
  @override
  createState() => new LoginWidgetState();
}

class LoginWidgetState extends State<LoginScreen> {
  bool isSecondPage;

  @override
  void initState() {
    super.initState();
    textControllerPhone.text = _phone;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void mySetState(bool isSecondPage) {
      if (_isSecondPage) {
        showSnackBarWithOutButton("_isSecondPage = true", appWhiteTextColor,
            snackBarErrorColor, scaffoldKey);
      } else {
        showSnackBarWithOutButton("_isSecondPage = false", appWhiteTextColor,
            snackBarErrorColor, scaffoldKey);
      }
      setState(() {
        _isSecondPage = isSecondPage;
      });
    }

    return new Scaffold(
      key: scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            backgroundGradientColorStart,
            backgroundGradientColorEnd
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),
        child: myAnimatedSwitcherWidget(mySetState),
      ),
    );
  }
}

Widget myAnimationStateWidget(Function function) {
  return ContentWidget(function);
  //  if (!_isSecondPage) {
//    return ContentWidget(function);
//  } else {
////    return LoadingWidget();
//  }
}

Widget myAnimatedSwitcherWidget(Function function) {
  return AnimatedSwitcher(
    transitionBuilder: (Widget child, Animation<double> animation) {
      return ScaleTransition(child: child, scale: animation);
    },
    child: myAnimationStateWidget(function),
    duration: Duration(milliseconds: 200),
  );
}

class ContentWidget extends StatelessWidget {
  final Function changeState;

  ContentWidget(this.changeState);

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double height = MediaQuery.of(context).size.height;
    double contentHeight = height / 10.0;

    ListView listView = ListView(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(left: 76, right: 76, top: 130),
            width: double.infinity,
            height: contentHeight,
            child: myLayoutSignUpLogo()),
        Container(
          width: double.infinity,
          height: contentHeight * 5,
          child: loginFragmentSelect(context, changeState),//loginFirstFragment(context, changeState),
//          child: loginSecondFragment(),
//          child: loginLastFragment(),
        )
      ],
    );
    return listView;
  }
}

Widget loginFragmentSelect(BuildContext context, Function changeState) {
  if (_isSecondPage) return loginSecondFragment();
  else return loginFirstFragment(context, changeState);
}

Widget loginFirstFragment(BuildContext context, Function changeState) {
  return InkWell(
      child: Container(
          margin: EdgeInsets.only(left: 32, right: 32, top: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 0.0),
                child: appTextFieldUrl(
                    "Открой доступ к самой полной базе рынка квартир в Сочи!",
                    Colors.white),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0),
                child: Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child: applicationButton(buttonGradientColorStart,
                        buttonGradientColorEnd, Colors.white, "Войти", () {
                      openNextPage(context, changeState);
                    })),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: RichText(
                  text: new TextSpan(
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Впервые у нас ? ',
                          style: TextStyle(fontSize: 13.0, color: Colors.grey)),
                      new TextSpan(
                          text: 'Регистрация',
                          style:
                              TextStyle(fontSize: 13.0, color: Colors.white)),
                    ],
                  ),
                ),
              )
            ],
          )));
}

Widget loginSecondFragment() {
  return InkWell(
    child: Container(
//                alignment: Alignment.center,
        margin: EdgeInsets.only(left: 32, right: 32, top: 35),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 0.0),
              child: appTextFieldWithoutImage(textFieldGradientColorStart,
                  textFieldGradientColorEnd, "Телефон", false, (String text) {
                _phone = text;
              }),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: Container(
                  alignment: Alignment.center,
                  child: applicationButton(buttonGradientColorStart,
                      buttonGradientColorEnd, Colors.white, "Далее", () {})),
            ),
          ],
        )),
  );
}

Widget loginLastFragment() {
  return InkWell(
      child: Container(
          margin: EdgeInsets.only(left: 32, right: 32, top: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 0.0),
                child: appTextFieldUrl(
                    "Введите код который мы отправили на указаный вами номер телефона",
                    Colors.white),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: appTextFieldWithoutImage(
                    textFieldGradientColorStart,
                    textFieldGradientColorEnd,
                    "Код в СМС",
                    false, (String text) {
                  _smsCode = text;
                }),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: Container(
                    alignment: Alignment.center,
                    child: applicationButton(buttonGradientColorStart,
                        buttonGradientColorEnd, Colors.white, "Войти", () {})),
              ),
            ],
          )));
}

Widget myLayoutSignUpLogo() {
  return InkWell(
    child: Container(
//      width: double.infinity,
      height: 40.0,
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 6.0, right: 6.0, top: 12.0),
            child: Image.asset('assets/images/img_swipe_logo.png',
                width: 75.0, height: 60.0),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text("свайп",
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 44,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
          )
        ],
      ),
    ),
  );
}

void sendCodeRequest(BuildContext context, Function function) {
  FocusScope.of(context).unfocus();
//  _isLoading = true;
  function(true, false);
  showSnackBarWithOutButton(
      "wrong data", appWhiteTextColor, snackBarErrorColor, scaffoldKey);
  function(false, false);
}

void openNextPage(BuildContext context, Function changeState) {
  FocusScope.of(context).unfocus();
  _isSecondPage = !_isSecondPage; //true;
  changeState(true);
}
