import 'package:flutter/material.dart';

import 'appStyle.dart';

//snackBar colors

String developing = "In developing";
String loginToTheApp = "Login to the app first";
String snackBarSuccess = "Success";
String snackBarWarning = "Warning";
String snackBarError = "Error";
String snackBarInfo = "info";

const Color snackBarSuccessColor = Color(0xff3CA455);
const Color snackBarErrorColor = Color(0xffC84040);
const Color snackBarWarningColor = Color(0xffE67337);
const Color snackBarInfoColor = Color(0xff0000E0);

void showSnackBarWithOutButton(String text, Color textColor, Color backgroundColor, GlobalKey<ScaffoldState> key) {
  final snackBar = SnackBar(
    content: Text(text, style: TextStyle(color: textColor)),
    backgroundColor: backgroundColor,
  );
  key.currentState.showSnackBar(snackBar);
}

void showSnackBarWithButton(String text, Color textColor, Color backgroundColor, GlobalKey<ScaffoldState> key, String buttonText, EmptyCallback callback) {
  final snackBar = SnackBar(
    content: Text(text, style: TextStyle(color: textColor)),
    backgroundColor: backgroundColor,
      action: SnackBarAction(
        label: buttonText,
        textColor: textColor,
        onPressed: () {
          callback();
        },
      )
  );
  key.currentState.showSnackBar(snackBar);
}