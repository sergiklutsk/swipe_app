import 'package:flutter/material.dart';
import 'package:swipeapp/screen_navigation_value.dart';
import 'package:swipeapp/styles/appStyle.dart';
import 'package:swipeapp/ui/loginScreen.dart';

void main() => runApp(
  new MaterialApp(
      debugShowCheckedModeBanner: false,
//      initialRoute: loginScreenPage2,
      initialRoute: loginScreen,
      theme: ThemeData(
//        brightness: Brightness.light,
        accentColor: colorAccent,
//        accentColorBrightness: Brightness.light,
      ),
      routes: {
//        loginScreenPage2: (BuildContext context) => LoginScreenPage2()
        loginScreen: (BuildContext context) => LoginScreen()
      }),
);
